# Italienisch lernen mit youtube
[zum video](https://www.youtube.com/watch?v=mR-FqrAQ6HY)

+ Guten Morgen -- Buongiorno
+ Guten Tag -- Buon Pomeriggio
+ Guten Abend -- Buona Sera
+ Gute Nacht -- Buona Notta
+ Wie geht es dir -- Come stai?
+ Wie geht es ihnen -- Come sta?
+ Wie geht's? -- Come va?
+ Gut, danke. -- Sto bene. Grazie.
+ Nicht schlecht -- Niente male.
+ Es geht mir sehr gut -- Sto molto bene.
+ Nicht gut -- No molto bene.
+ Auf Wiedersehen -- Arrivederci
+ Tschüss -- Ciao
+ Bis später -- Ci vediamo dopo
+ Und ihnen? -- E lei?
+ Was hast du so gemacht -- Che hai fatto (in questo periodo)?
+ Nicht viel -- Niente di che
+ Ich habe viel zu tun gehabt -- Sono stato molto impegnato
+ Freut mich -- Piacere di conoscerti!
+ Bis bald -- Ci vediamo presto!
+ Bis morgen -- Ci vediamo domani!
+ Machs gut -- Stammi bene
+ Ja -- Si
+ Nein -- No
+ Ok -- Ok
+ Vielleicht -- Forse
+ Danke -- Grazie
+ Vielen Dank -- Grazie mille
+ Bitteschön -- Prego
+ Bitte -- Per favore
+ Entschuldigung -- Mi scusi
+ Entschuldigung, wie spät ist es bitte? -- Scusi, che ore sono?
+ Es tut mir leid -- Mi dispiace.
+ Kein Problem -- Nessun problem
+ Das macht nichts -- Non importa
+ Sprichst du arabisch -- Parli Arabo?
+ Sprechen sie englisch -- Parli Inglese?
+ Sprechen sie deutsch -- Parli Tedesco?
+ Nur ein bisschen -- Un po'.
+ Ich spreche kein Deutsch -- Non parlo Tedesco
+ Ich kann Englisch sprechen -- Parlo Inglese
+ Mein Englisch ist nicht sehr gut -- Il mio Inglese non è ottimo
+ Ich weiß nicht -- No lo so.
+ Haben sie das verstanden -- Capisci?
+ Ich verstehe -- Capisco
+ Ich verstehe nicht -- Non capisco
+ Könnten sie das bitte wiedeholen -- Puoi ripetere per favore?
+ Können sie bitte etwas langsamer sprechen -- Puoi parlare più lentamente per favore?
+ Wass ist das hier -- Cos'è questo?
+ Was ist das -- Cos'è quello?
+ Schreiben sie es bitte auf -- Per favore scrivilo!
+ Wie sagt man "hello" auf deutsch -- Come si dice "ciao" in Tedesco?
+ Wo bist du -- Dove sei?
+ Ich bin zu Hause -- Sono a casa
+ Ich bin auf der Arbeit -- Sono a lavoro
+ Was machen sie beruflich -- Che lavoro fai?
+ Ich bin Lehrer -- Sono un/una insegnante
+ Ich bin Student -- Sono uno/una studente/studentessa.
+ Ich möchte Englisch lernen -- Voglio imparare l'Inglese.
+ Würden sie mir bitte helfen -- Mi aiuteresti per favore?
+ Können sie mir helfen -- Puoi aiutarmi?
+ Kann ich ihnen helfen -- Posso aiutarti?
+ Ich habe mich verlaufen -- Mi sonon perso/persa!
+ Einen augenblick -- Un momento, per favore
+ Wieviel kostet das -- Quanto costa?
+ Wie heißen sie -- Qual è il suo nome?
+ Wie heißt du? -- Come ti chiami?
+ Mein name ist John -- Mi chiamo John.
+ Ich bin Jon -- Sono John.
+ Woher kommen sie -- Di dove è lei?
+ Woher kommst du? -- Di dove sei?
+ Welche Nationalität haben sie -- Qual è la tua/sua nazionalità?
+ Ich komme aus den USA -- Vengo dagli Stati Uniti.
+ Ich komme aus Spanien -- Vengo dalla Spagna.
+ Wo wohnen sie? -- Dove vive?
+ Wo wohnst du? -- Dove vivi?

[weiter geht es hier:](https://youtu.be/mR-FqrAQ6HY?t=1644)
